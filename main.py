# pylint: disable=C0103, C0111

import time
import copy
import pygame
import numpy as np
from random import random
from car import Car
from track import Track
from board import Board
from ai import AI

# Main parameters
width = 1200
height = 800

speed = 30

nr_cars = 7
nr_corners = 12
track_width = 100

# Initialize
pygame.init()
pygame.font.init()
clock = pygame.time.Clock()
main_s = pygame.display.set_mode((width, height))
track = Track(nr_corners, track_width)
track.generate(main_s)
max_score = -10
iteration = 0

cars = []
for i in range(nr_cars):
    seed = int(100*random())
    cari = Car(track.checkpoint[0], speed, AI(5, 3, seed))
    cars.append(cari)

#board = Board()

def crashed(surface):
    # Display crashed message
    largeText = pygame.font.Font('freesansbold.ttf', 65)
    text = "Car crashed"
    TextSurf = largeText.render(text, True, (0, 0, 0))
    TextRect = TextSurf.get_rect()
    TextRect.center = ((width/2), (height/2))
    surface.blit(TextSurf, TextRect)

    pygame.display.update()

    time.sleep(2)

    pygame.quit()
    exit()

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            print('Iteration ' + str(iteration))
            print(max_score)
            print(maxbrain.weights1)
            print(maxbrain.weights2)
            running = False
            pygame.quit()
            exit()

    main_s.fill((250, 250, 250))

    track.display(main_s)

    # update car
    total_life = 0
    for car in cars:
        car.update(track)
        car.display(main_s)
        total_life += car.life

    if total_life <= 0:
        iteration += 1
        # all cars dead
        for car in cars:
            if car.score > max_score:
                max_score = car.score
                maxcar = copy.deepcopy(car)
                maxbrain = maxcar.brain

        #track = Track(nr_corners, track_width)
        #track.generate(main_s)

        #maxcar.resurrect(track.checkpoint[0], speed)

        cars = []
        for i in range(nr_cars-1):
            seed = int(100*random())
            cari = Car(track.checkpoint[0], speed, AI(5, 3, seed))
            cari.brain.update(maxcar.brain)
            cars.append(cari)

    # update scoreboard
    #board.display(main_s, car)

    # refresh screen
    pygame.display.update()
    clock.tick(speed)
