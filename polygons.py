# pylint: disable=C0103, C0111

import numpy as np

def in_polygon(pos, poly):
    # Determines if a point is inside a polygon by checking if the line
    # from 0,y to x,y crosses an odd number of polygon lines.

    # Count intersected lines
    pos_line = ((0, pos[1]), pos)
    count = 0
    for p, point in enumerate(poly):
        if p == len(poly)-1:
            if point[0] == poly[0][0] and point[1] == poly[0][1]:
                break
            line = (point, poly[0])
        else:
            line = (point, poly[p+1])

        if intersect_line(pos_line, line):
            count += 1
    if count % 2 == 0:
        return False
    return True

def knn_search(pos, pos_list, k):
    # Finds the k nearest points in pos_list for pos
    dist = [np.sqrt((point[0] - pos[0])**2 + (point[1] - pos[1])**2) for point in pos_list]
    sortedDist = np.argsort(dist)
    return sortedDist[0:k]

def in_polygon_min(edge, poly):
    # Determines if an edge is in a polygon by checking for intersections
    # Should be faster than counting them

    for p, point in enumerate(poly):
        line = (point, poly[(p+1) % len(poly)])
        if intersect_line(edge, line):
            return False
    return True

def intersect_line(line1, line2):
    # Determines if two lines intersect by comparing the side of each end point to each line.
    if (side(line1[0], line2) != side(line1[1], line2)) and (side(line2[0], line1) != side(line2[1], line1)):
        return True
    return False

def side(pos, line):
    # Determines the side of which a point lies with respect to a line.
    # positive area -> left, negative area -> right, zero area -> collinear.
    if area(pos, line) > 0:
        sid = 1
    elif area(pos, line) < 0:
        sid = -1
    else:
        sid = 0
    return sid

def area(pos, line):
    # Determines the area of a triangle of a point and the two endpoints of a line.
    a = pos
    b = line[0]
    c = line[1]
    return (np.cross(a, b) + np.cross(b, c) + np.cross(c, a))/2

def find_intersect(line, ray):
    # Finds point of intersection between a line and an infinite ray
    (x1, y1), (x2, y2) = line
    (x3, y3), (x4, y4) = ray

    den = (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4)
    if den == 0:
        # paralel lines
        return (np.inf, np.inf)
    t = ((x1 - x3)*(y3 - y4) - (y1 - y3)*(x3 - x4))/den
    u = - ((x1 - x2)*(y1 - y3) - (y1 - y2)*(x1 - x3))/den

    if 0 <= t <= 1 and u >= 0:
        #intersection falls within line length
        return ((x1 + t*(x2 - x1)), (y1 + t*(y2 - y1)))
    return (np.inf, np.inf)

def distance(point1, point2):
    # Finds euclidean distance between 2 points
    x1, y1 = point1
    x2, y2 = point2

    return np.sqrt((x1 - x2)**2 + (y1 - y2)**2)
