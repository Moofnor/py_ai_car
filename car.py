# pylint: disable=C0103, C0111

import pygame
import numpy as np
from polygons import intersect_line, in_polygon_min, knn_search, find_intersect, distance


class Car:
    def __init__(self, pos, speed, brain):
        self.width = 5
        self.height = 10

        self.angle = 0
        self.rays = [-45, -22, 0, 22, 45]
        self.dist = [0, 0, 0, 0, 0]
        self.intersect = [(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)]

        self.center = pos
        self.coords = findCoords(self.center, self.width, self.height, self.angle)

        self.velocity = 150 / speed
        self.dangle = 300 / speed

        self.score = 0
        self.next_point = 1

        self.brain = brain
        self.life = 1

    def update(self, track):
        # Check if on track
        if not on_track(self, track):
            self.life = 0
            return

        if self.life <= 0:
            self.velocity = 0
            self.life = 0
            return

        # Check if checkpoint is crossed
        if intersect_line((self.coords[0], self.coords[2]), (track.inner[self.next_point], track.outer[self.next_point])):
            self.score += 10
            self.life = 1
            self.next_point += 1
            if self.next_point > track.nr_corners - 1:
                self.next_point = 0

        rad_a = np.deg2rad(self.angle)

        x_vel = np.sin(rad_a)*self.velocity
        y_vel = np.cos(rad_a)*self.velocity

        # update location
        self.center = (self.center[0] + x_vel, self.center[1] + y_vel)
        self.coords = findCoords(self.center, self.width, self.height, self.angle)

        # update views of rays
        self.dist, self.intersect = ray_check(self, track)

        # update next action
        self.brain.feedforward(self.dist)
        # 1 output
        #self.angle += (-1)**round(float(self.brain.output)) * self.dangle      # l r 
        #self.angle += (round(2 * float(self.brain.output) - 1)) * self.dangle  #l c r

        # 3 ouputs
        action = np.argsort(self.brain.output)
        if action[0] == 0:
            self.angle += self.dangle
        elif action[0] == 2:
            self.angle -= self.dangle

        self.life -= 0.01
        self.score += 0.01

    def display(self, main_surface):
        pygame.draw.polygon(main_surface, (20, 120, 100), self.coords, 0)

       # for point in self.intersect:
       #     pygame.draw.line(main_surface, (230, 30, 30), (int(self.center[0]), int(self.center[1])), (int(point[0]), int(point[1])), 1)
       #    pygame.draw.circle(main_surface, (230, 30, 230), (int(point[0]), int(point[1])), 2, 0)

    def resurrect(self, pos, speed):
        self.angle = 0
        self.dist = [0, 0, 0, 0, 0]

        self.center = pos
        self.coords = findCoords(self.center, self.width, self.height, self.angle)

        self.velocity = 150 / speed

        self.score = 0
        self.next_point = 1

        self.life = 1

def findCoords(center, width, height, angle):

    half_w = width/2
    half_h = height/2
    rad_a = np.deg2rad(angle)

    # radius and angle of the corners
    self_r = np.sqrt(half_w**2+half_h**2)
    self_a = np.arctan(half_w/half_h)

    front_l = (center[0] - self_r*np.sin(-rad_a + self_a), center[1] + self_r*np.cos(-rad_a + self_a))
    front_r = (center[0] + self_r*np.sin(rad_a + self_a), center[1] + self_r*np.cos(rad_a + self_a))
    rear_l = (center[0] - self_r*np.sin(rad_a + self_a), center[1] - self_r*np.cos(rad_a + self_a))
    rear_r = (center[0] + self_r*np.sin(-rad_a + self_a), center[1] - self_r*np.cos(-rad_a + self_a))

    return (front_l, front_r, rear_r, rear_l)

def on_track(car, track):
    # find closest point in inner
    id_inner = int(knn_search(car.center, track.inner, 1))
    id_outer = int(knn_search(car.center, track.outer, 1))
    ln = len(track.inner)

    inner = [track.inner[i] for i in ( id_inner-1, id_inner, (id_inner+1) % ln)] 
    outer = [track.outer[i] for i in ( id_outer-1, id_outer, (id_outer+1) % ln)]

    lines = ((inner[0], inner[1]),(inner[1], inner[2]),(outer[0], outer[1]),(outer[1], outer[2]))

    pos = car.coords
    for c in range(4):
        edge = (pos[c], pos[(c+1) % 4])
        for line in lines:
            if intersect_line(edge, line):
                return False
    return True

def ray_check(car, track):
    # returns intersection of each sensor ray, and the distance to intersection
    ray_dist = car.dist
    ray_intersect = car.intersect
    for i, angle in enumerate(car.rays):
        look_angle = np.deg2rad(angle + car.angle)
        ray = (car.center, (np.add(car.center, (20 * np.sin(look_angle), 20 * np.cos(look_angle)))))

        min_dist = np.inf
        min_intersect = ray[1]

        for p, inner in enumerate(track.inner):
            line = (inner, track.inner[(p+1) % track.nr_corners])
            intersect = find_intersect(line, ray)
            if intersect != (np.inf, np.inf):
                dist = distance(intersect, car.center)
                if dist < min_dist:
                    min_dist = dist
                    min_intersect = intersect

        for p, outer in enumerate(track.outer):
            line = (outer, track.outer[(p+1) % track.nr_corners])
            intersect = find_intersect(line, ray)
            if intersect != (np.inf, np.inf):
                dist = distance(intersect, car.center)
                if dist < min_dist:
                    min_dist = dist
                    min_intersect = intersect

        ray_dist[i] = min_dist
        ray_intersect[i] = min_intersect
    return ray_dist, ray_intersect
