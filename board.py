# pylint: disable=C0103, C0111
import pygame

class Board:
    def __init__(self):
        self.size = (100, 150)
        self.pos = (0, 0) #left top
        self.Rect = pygame.Rect(self.pos, self.size)

        self.back_color = (70, 70, 70)
        self.text_color = (160, 160, 250)
        self.title_color = (140, 140, 230)
        self.font = pygame.font.Font('freesansbold.ttf', 12)

    def display(self, surface, obj):
        # x, y, score, next checkpoint
        titles = ('x', 'y', 'score', 'check', 'dist_l', 'dist_c', 'dist_r')
        values = (obj.center[0], obj.center[1], obj.score, obj.next_point, obj.dist[0], obj.dist[1], obj.dist[2])

        pygame.draw.rect(surface, self.back_color, self.Rect, 0)

        for n, title in enumerate(titles):
            render(title, (self.pos[0] + 10, self.pos[1] + n*20 + 10), self.title_color, self.font, surface)
            render(str(values[n])[0:5], (self.pos[0] + 50, self.pos[1] + n*20 + 10), self.text_color, self.font, surface)

        pygame.display.update()

def render(text, pos, color, font, surface):
    Surf = font.render(text, True, color)
    Rect = Surf.get_rect()
    Rect.topleft = pos
    surface.blit(Surf, Rect)
