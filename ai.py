# pylint: disable=C0103, C0111

import numpy as np

class AI:
    def __init__(self, sizex, sizey, seed):
        self.seed = seed
        np.random.seed(seed)
        self.size = [sizex, sizey]

        self.layer = np.zeros(8)
        self.output = np.zeros(sizey)
        self.weights1 = np.random.rand(sizex, self.layer.shape[0])
        self.weights2 = np.random.rand(self.layer.shape[0], sizey)
        self.learningrate = 0.05

    def feedforward(self, x):
        x = normalize(x)
        self.layer = sigmoid(np.dot(x, self.weights1))
        self.output = sigmoid(np.dot(self.layer, self.weights2))

    def update(self, parent):
        if np.random.rand(1) > 0.8:
            learningrate = 0.6
        else:
            learningrate = self.learningrate

        mutation1 = learningrate * np.random.rand(self.size[0], self.layer.shape[0])
        mutation2 = learningrate * np.random.rand(self.layer.shape[0], self.size[1])

        self.weights1 = np.add((1 - learningrate) * parent.weights1, mutation1)
        self.weights2 = np.add((1 - learningrate) * parent.weights2, mutation2)

    def setweights(self, w1, w2):
        self.weights1 = np.array(w1)
        self.weights2 = np.array(w2)

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def normalize(x):
    xmin = np.min(x)
    xmax = np.max(x)
    return np.array([(i - xmin) / (xmax - xmin) for i in x])
