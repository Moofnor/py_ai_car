# pylint: disable=C0103, C0111

from random import random
import pygame
import numpy as np

class Track:
    def __init__(self, corners, width):

        self.checkpoint = []
        self.inner = []
        self.outer = []
        self.nr_corners = corners
        self.track_width = width


    def generate(self, main_surface):
        height = main_surface.get_height()
        width = main_surface.get_width()

        track_center = (np.floor(width/2), np.floor(height/2))

        track_radius = 0.5 * np.min((height, width)) - 1.5 * self.track_width

        # Define random track by taking a set angle, and a random radius per corner
        for n in range(self.nr_corners):
            corner_angle = n/self.nr_corners * 2 * np.pi
            corner_radius = track_radius + 2 * self.track_width * (random() - 0.5)

            corner_delta = np.dot(corner_radius, (np.cos(corner_angle), np.sin(corner_angle)))
            inner_delta = np.dot(corner_radius - 0.5 * self.track_width, (np.cos(corner_angle), np.sin(corner_angle)))
            outer_delta = np.dot(corner_radius + 0.5 * self.track_width, (np.cos(corner_angle), np.sin(corner_angle)))

            self.checkpoint.append(np.floor(np.add(track_center, corner_delta)))
            self.inner.append(np.floor(np.add(track_center, inner_delta)))
            self.outer.append(np.floor(np.add(track_center, outer_delta)))

    def display(self, main_surface):
        # Draw inner and outer boundaries of track
        pygame.draw.lines(main_surface, (70, 70, 70), True, self.inner, 3)
        pygame.draw.lines(main_surface, (70, 70, 70), True, self.outer, 3)

        # Draw checkpoint lines
        for n in range(self.nr_corners):
            pygame.draw.line(main_surface, (30, 30, 30), self.inner[n], self.outer[n], 1)
        